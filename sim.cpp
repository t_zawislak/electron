#include "sim.hpp"

double hamiltonian( double* _psi, int _k, const Parameters &_p, double _tau)  {
  if(_k==0 || _k==_p.N) return 0.;
  double h = -0.5*(_psi[_k+1] + _psi[_k-1] - 2.*_psi[_k]) / ((1./_p.N)*(1./_p.N));
  h += _p.kappa*( static_cast<double>(_k)/_p.N - 0.5) * _psi[_k] * sin(_p.omega * _tau) ;
  return h;
}



void krok( double* _psiR, double* _psiI, const Parameters &_p, State &_st, int _obliczNXE, int _wypiszRHO){
  double t_psiR[_p.Np1];
  double t_psiI[_p.Np1];
  t_psiR[0] = 0;
  t_psiI[0] = 0;
  t_psiR[_p.N] = 0;
  t_psiI[_p.N] = 0;

  for( int k=1; k<_p.N; ++k )    t_psiR[k] =  _psiR[k] +  _p.dtau/2. * hamiltonian(_psiI, k, _p, _st.tau);
  for( int k=1; k<_p.N; ++k )    t_psiI[k] =  _psiI[k] -  _p.dtau    * hamiltonian(t_psiR, k, _p, _st.tau + 0.5*_p.dtau) ;
  for( int k=1; k<_p.N; ++k )    t_psiR[k] = t_psiR[k] +  _p.dtau/2. * hamiltonian(t_psiI, k, _p, _st.tau + _p.dtau);
  for( int k=0; k<_p.Np1; ++k ){
    _psiR[k] = t_psiR[k];
    _psiI[k] = t_psiI[k];
  }
  _psiR[0] = 0;
  _psiI[0] = 0;
  _psiR[_p.N] = 0;
  _psiI[_p.N] = 0;


  Normalize(_psiR, _psiI, _p);

  if(!_obliczNXE){
    _st.NN=0; _st.XX=0; _st.EE=0;
   for( int k=0; k<_p.Np1; ++k ){
      _st.NN += 1./_p.N * (_psiR[k]*_psiR[k] + _psiI[k]*_psiI[k]);
      _st.XX += 1./_p.N * static_cast<double>(k)/_p.N * (_psiR[k]*_psiR[k] + _psiI[k]*_psiI[k]);
      _st.EE += 1./_p.N * (_psiR[k]*hamiltonian(_psiR, k, _p, _st.tau) + _psiI[k]*hamiltonian(_psiI, k, _p, _st.tau));
    }
    _st.WypiszState(_p.pom_file, _st.tau);
  }

  if(!(_wypiszRHO-1)) _st.WypiszGestosc(_psiR, _psiI, _p.den_file );
}

void Normalize(double* _psiR, double* _psiI, const Parameters& _p){
  double norm = 0.;
  for( int k=0; k<_p.Np1; ++k )
    norm += 1./_p.N * (_psiR[k]*_psiR[k] + _psiI[k]*_psiI[k]);
  norm = sqrt(norm);


  for( int k=0; k<_p.Np1; ++k ){
    _psiR[k] /= norm;
    _psiI[k] /= norm;
  }


}




/*


  State


*/

State::State() : N(0){}

State::State(int _Np1) : N(_Np1-1){
  tau=0.;
}

State::~State(){
}

void State::WypiszState(FILE* _file, const double tau){
  fprintf(_file, "%.3le\t%.3le\t%.3le\t%.3le\n", tau, NN, XX, EE );
}

void State::WypiszGestosc(double *_psiR, double *_psiI, FILE* _file){
  for(int i=0; i<N+1; i++) fprintf(_file, "%.3le\t%.3le\n", static_cast<double>(i)/N, _psiR[i]*_psiR[i] + _psiI[i]*_psiI[i] );
  fprintf(_file, "\n");
}

/*


  Parameters


*/


Parameters::~Parameters(){
  	fclose(den_file);
  	fclose(pom_file);
}

Parameters::Parameters(const std::string &_param_path){
  FILE* file_params = fopen(_param_path.c_str(), "r");
	char s[256];      // comment to be discarded

	fscanf(file_params, "%i %s", &n, s);
	fscanf(file_params, "%i %s", &N, s);
  fscanf(file_params, "%lf %s", &L, s);
  fscanf(file_params, "%lf %s", &omega, s);
  fscanf(file_params, "%lf %s", &dtau, s);
  fscanf(file_params, "%lf %s", &kappa, s);
  fscanf(file_params, "%lf %s", &tauMax, s);
  fscanf(file_params, "%d %s", &S_NXC, s);
  fscanf(file_params, "%d %s", &S_rho, s);


  Np1 = N+1;


  den_file = fopen( path_density.c_str(), "w" );
  pom_file = fopen( path_pomocnicze.c_str(), "w" );

	fclose(file_params);
}









//
