#ifndef _sim_hpp_
#define _sim_hpp_

#include <iostream>
#include <complex>
#include <math.h>

#define PI   3.14159265
#define SQ2  1.41421356




typedef std::complex<double> Complex;

class Parameters{
  public:
    int N, Np1, n; //n - mody
    int S_NXC, S_rho;
    double L, dtau, tauMax;
    double kappa, omega;
    const std::string path_density="output/density.dat";
    const std::string path_pomocnicze="output/pomocnicze.out";
    FILE* den_file;
    FILE* pom_file;
    Parameters(const std::string &param_path);
    ~Parameters();
};

class State{
  public:
    const int N;  // to samo N co w Params
    double rho=0, meanX=0, meanE=0;
    double NN=0, XX=0, EE=0;
    double tau;

    State();
    State(int _N);
    ~State();

    void WypiszState(FILE* _file, const double tau);
    void WypiszGestosc(double *_psiR, double *_psiI, FILE* _file);

};

void Normalize(double*, double*, const Parameters&);

double hamiltonian( double* _psi, int _k, const Parameters &_p, double _tau) ;

double Vxt( const Parameters _p, State &_st);

void   krok(double*, double*,  const Parameters &_p, State &_st, int _obliczNXE, int _wypiszRHO);



#endif
