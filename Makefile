CXX = g++
CXXFLAGS =  -Wall -pedantic -std=c++11

SRCS = main.cpp sim.cpp
OBJS = $(SRCS:.c=.o)


all: schrodinger

schrodinger: $(OBJS)
	$(CXX) $(CXXFLAGS) -o $@ $^

clean:
	rm -f *.o schrodinger

.PHONY: all clean
