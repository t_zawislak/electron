import matplotlib.pyplot as plt
import numpy as np
import time

pomocnicze = np.loadtxt("output/pomocnicze.out")
#plot 1:
tau = pomocnicze[:, 0]
N = pomocnicze[:, 1]
X = pomocnicze[:, 2]
E = pomocnicze[:, 3]



fig, axes = plt.subplots(2, 2)

plt.subplot(2,2,1)
plt.plot(tau,N)
plt.xlabel(r'$\tau$')
plt.ylabel('N')

plt.subplot(2,2,2)
plt.plot(tau,X)
plt.xlabel(r'$\tau$')
plt.ylabel('X')

plt.subplot(2,2,3)
plt.plot(tau,E)
plt.xlabel(r'$\tau$')
plt.ylabel('E')

plt.subplot(2,2,4)

den_file = np.loadtxt("output/density.dat")
x = den_file[:101, 0]
den0 = den_file[:101, 1]
denF = den_file[-101:, 1]


plt.plot(x,den0)
plt.ylabel(r'$\rho$')
plt.xlabel('x')

plt.plot(x,denF)



plt.show()
