
#include "sim.hpp"
#include <iomanip>

int main(int argc, char* argv[] ){

  Parameters par("input/params.txt");
  State st(par.Np1);
  double psiR[par.Np1];
  double psiI[par.Np1];

  // initial sate
  for(int k=0; k<par.Np1; ++k){
    psiR[k] = SQ2 * sin( par.n*PI*( static_cast<double>(k)/par.N ) ) ;
    psiI[k] = 0. ;
  }

  int iterator = 1;

  while( st.tau < par.tauMax ){
    //st.WypiszGestosc( par.den_file );
    krok(psiR, psiI, par, st, iterator % par.S_NXC, iterator % par.S_rho);

    st.tau += par.dtau;
    std::cout << std::setprecision(2)<< "Progress:\t" << st.tau / (par.tauMax) *100 << std::setprecision(6)<< "%             \r";
    ++iterator;
  }
  st.WypiszGestosc(psiR, psiI, par.den_file );

  return 0;
}
